<?php
class Helpers {
    public $hostAndPort = "http://localhost:8009/swapitest/";

    public function fetchAllCharacters() {
        $url = "https://swapi.co/api/people/?format=json";
        list($status) = get_headers($url);
        if (strpos($status, '200') == TRUE) {
            $temp=file_get_contents($url);
            $temp= json_decode($temp,TRUE);
            return $temp['results'];
        }else{
            return 'No network connection to API URL! '.$status;
        }
    }

    public function fetchSingleCharacter($url) {
        list($status) = get_headers($url);
        if (strpos($status, '200') == TRUE) {
            $temp=file_get_contents($url);
            $temp= json_decode($temp,TRUE);
            return $temp;
        }else{
            return 'No network connection to API URL! '.$status;
        }
    }

    public function convertDate($parsedString) {
        $parsedString = strtotime($parsedString);
        return date("l F jS, Y - g:ia", $parsedString);
    }

    public function saveFavourites($characterId){
        $temp=file_get_contents(__DIR__.('\storage\favourites.json'));
        $temp = json_decode($temp,TRUE);

        file_put_contents(__DIR__.('\storage\favourites.json'), "");
        if (!empty($temp)) {
            foreach ($temp as $key => $item) {
                if($item == $characterId){
                    $newArray = array($characterId);
                    $temp = array_diff($temp, $newArray);
                }else{
                    $newArrayItem = $characterId;
                    $temp[] = $newArrayItem;
                }
            }
            file_put_contents(__DIR__.('\storage\favourites.json'), json_encode(array_unique($temp)));
        }else{
            $temp[] = $characterId;
            file_put_contents(__DIR__.('\storage\favourites.json'), json_encode(array_unique($temp)));
        }

        return 1;
    }

    public function getIfIsFavourite($id)
    {
        $temp=file_get_contents(__DIR__.('\storage\favourites.json'));
        $temp = json_decode($temp,TRUE);
        $defaultVal = 0;
        foreach ($temp as $key => $value) {
            if ($value == $id){
                $defaultVal = 1;
            }
        }
        return $defaultVal;
    }

}
