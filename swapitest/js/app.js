$(document).ready(function(){

    $('body').on('click', '#view-single-character', function (a) {
        a.preventDefault();
        var characterId = $(this).attr('data-id');
        var processUrl = $(this).attr('data-url');

        $.ajax({
            type: 'post',
            url: processUrl,
            data: {
                characterId: characterId
            },
            success: function(data){
                $('.modal-content').html(data);
            },
            error: function (err) {
                $('.modal-content').html(err);
            }
        })
    });

    $('body').on('click', '#heart-show-favourite', function (a) {
        a.preventDefault();
        var dataId = $(this).attr('data-id');
        var processUrl = $(this).attr('data-url');

        $.ajax({
            type: 'post',
            url: processUrl,
            data: {
                dataId: dataId
            },
            success: function (data) {
                console.log(data);
                location.reload();
            },
            error: function (err) {
                alert(err)
            }
        })
    })

});