<?php
include "header.php";
?>
<?php
    if(is_array($helpers->fetchAllCharacters())) {
        $fetchAllStarsArray = $helpers->fetchAllCharacters();
        //print_r($fetchAllStarsArray);
        ?>
        <table cellpadding="7" cellspacing="7">
            <tr>
                <th>NUMBER</th>
                <th>NAME</th>
                <th>VIEW</th>
                <th>FAVOURITE</th>
            </tr>
            <?php
            $i = 1;
            $j = 1;
            $k = 1;
            $l = 1;
            foreach ($fetchAllStarsArray as $item) {
                ?>
                    <tr>
                        <td>
                            <?php
                                echo ($i++).'. ';
                            ?>
                        </td>
                        <td>
                            <?php echo strtoupper(($item['name']))?>
                        </td>
                        <td>
                            <a id="view-single-character" href="javascript:;" data-id="<?php echo $j++ ?>"
                               data-url="<?php echo $helpers->hostAndPort.'view.php'?>"
                               data-toggle="modal" data-target="#charcter-modal" class="btn btn-primary">
                                <i class="fa fa-mouse-pointer"></i> View
                            </a>
                        </td>
                        <td class="favourite-td">
                            <a id="heart-show-favourite" data-url="<?php echo $helpers->hostAndPort.'favourite.php'?>"
                               data-id="<?php echo $k++?>" href="javascript:;">
                                <?php
                                    if ($helpers->getIfIsFavourite($l++) == 0) {
                                        ?>
                                        <i class="far fa-heart"></i>
                                        <?php
                                    }else{
                                        ?>
                                        <i class="fa fa-heart"></i>
                                        <?php
                                    }
                                ?>
                            </a>
                        </td>
                    </tr>
                <?php
                }
            ?>
        </table>
        <?php
    }else{
        echo ($helpers->fetchAllCharacters());
    }
?>
<?php
include "footer.php";
?>
