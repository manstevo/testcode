<?php
include "Helpers.php";
$helper = new Helpers();
$charId = $_REQUEST['characterId'];
$url = 'https://swapi.co/api/people/'.$charId.'/?format=json';
$response = $helper->fetchSingleCharacter($url);
if(is_array($response)) {
    ?>
        <div class="modal-header">
            <h4 class="modal-title">NAME: <?php echo strtoupper($response['name'])?></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
           <div class="profile-detail">
               <h5>Profile Details</h5>
               <ul>
                   <li>Height: <?php echo $response['height']?></li>
                   <li>Mass: <?php echo $response['mass']?></li>
                   <li>Hair Color: <?php echo $response['hair_color']?></li>
                   <li>Skin Color: <?php echo $response['skin_color']?></li>
                   <li>Eye Color: <?php echo $response['eye_color']?></li>
                   <li>Birth Year: <?php echo $response['birth_year']?></li>
                   <li>Gender: <?php echo $response['gender']?></li>
                   <li>Home World: <a href="<?php echo $response['homeworld']?>" target="_blank">View</a></li>
               </ul>
               <h5>Films</h5>
               <div class="details-array-div">
                   <ul>
                       <?php
                       $filmNo = 1;
                       foreach ($response['films'] as $film) {
                           ?>
                           <li><a href="<?php echo $film?>" target="_blank" class="btn btn-success">Open Film
                                   <?php echo $filmNo++?></a></li>
                           <?php
                       }
                       ?>
                   </ul>
               </div>
               <h5>Species</h5>
               <div class="details-array-div">
                   <ul>
                       <?php
                       $speciesNo = 1;
                       foreach ($response['species'] as $species) {
                           ?>
                           <li><a href="<?php echo $species?>" target="_blank" class="btn btn-info">Open Species
                                   <?php echo $speciesNo++?></a></li>
                           <?php
                       }
                       ?>
                   </ul>
               </div>
               <h5>Vehicles</h5>
               <div class="details-array-div">
                   <ul>
                       <?php
                       $vehiclesNo = 1;
                       foreach ($response['vehicles'] as $vehicle) {
                           ?>
                           <li><a href="<?php echo $vehicle?>" target="_blank" class="btn btn-primary">
                                   Open Vehicle <?php echo $vehiclesNo++?></a></li>
                           <?php
                       }
                       ?>
                   </ul>
               </div>
               <h5>Starships</h5>
               <div class="details-array-div">
                   <ul>
                       <?php
                       $starshipNo = 1;
                       foreach ($response['starships'] as $starship) {
                           ?>
                           <li><a href="<?php echo $starship?>" target="_blank" class="btn btn-info">
                                   Open Starship <?php echo $starshipNo++?></a></li>
                           <?php
                       }
                       ?>
                   </ul>
               </div>
               <h5>Other Details</h5>
               <div>
                   <ul>
                       <li> Created at: <?php echo $helper->convertDate($response['created'])?> </li>
                       <li> Last edited at: <?php echo $helper->convertDate($response['edited'])?> </li>
                       <li><a href="<?php echo $response['url']?>" target="_blank" class="btn btn-warning">Open URL </a></li>
                   </ul>
               </div>

           </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    <?php
}else{
    echo "<h3>".$response."</h3>";
}
?>