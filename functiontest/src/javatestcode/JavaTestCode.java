package javatestcode;

import functiontest.TestFunction;
import java.util.ArrayList;
import java.util.function.Function;

/**
 *
 * @author Stephen.Okoth
 */
public class JavaTestCode {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // ...test binary search begin
        ArrayList<Integer> arlist = new ArrayList<>();

        arlist.add(1);
        arlist.add(3);
        arlist.add(2);
        arlist.add(0);

        int index = TestFunction.search(arlist, 2);

        System.out.println("value is available at index: " + index);

        // ...test prime number with memoization begin
        TestFunction t = new TestFunction();

        int testnum = 7;
        boolean isprime = t.isPrimeMemo().apply(testnum);

        System.out.println("Is " + testnum + " a prime number? " + isprime);
    }
}
