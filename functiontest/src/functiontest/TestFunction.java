package functiontest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 *
 * @author Stephen.Okoth
 */
public class TestFunction {

    public static int search(ArrayList<Integer> arrlist, int tofind) {
        int index = 0;

        //sort the arraylist
        Collections.sort(arrlist);

        //search for the value from the arraylist collection
        index = Collections.binarySearch(arrlist, tofind, null);

        //return -1 if value not found from the arraylist
        if (index < 0) {
            index = -1;
        }

        return index;
    }

    // isPrime function without memoization
    public static boolean isPrime(int num) {
        boolean isprime = false;
        int i, m = 0, flag = 0;

        m = num / 2;

        if (num == 0 || num == 1) {
            //0 and 1 are not prime numbers
            isprime = false;
        } else {
            for (i = 2; i <= m; i++) {
                if (num % i == 0) {
                    //numbers greater than 2 whose modulus by 2 is 0 are not prime
                    isprime = false;
                    flag = 1;
                    break;
                }
            }

            //remaining numbers are prime
            if (flag == 0) {
                isprime = true;
            }
        }

        return isprime;
    }

    // memoize function that returns memoized version of the func function
    public static <T, R> Function<T, R> memoize(final Function<T, R> func) {
        Map<T, R> cache = new ConcurrentHashMap<>();
        return (t) -> cache.computeIfAbsent(t, func);
    }

    // memoizing the isPrime function
    public Function<Object, Boolean> isPrimeMemo() {
        return memoize((t) -> isPrime((int) t));
    }
}
